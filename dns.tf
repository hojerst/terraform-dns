locals {
  zones = toset([for s in fileset(var.zones_path, "*.csv") : trimsuffix(s, ".csv")])
  records = flatten([
    for zone in local.zones : [
      for record in csvdecode(file("${var.zones_path}/${zone}.csv")) :
      merge({ zone = zone }, record)
    ]
  ])
}

data "cloudflare_zone" "zones" {
  for_each = local.zones
  name     = each.value
}

resource "cloudflare_record" "records" {
  for_each = { for record in local.records : "${record.zone},${record.type},${record.name},${record.value}" => record }

  zone_id = data.cloudflare_zone.zones[each.value.zone].id
  name    = each.value.name
  content = each.value.type != "CAA" ? (
    each.value.type == "MX" ?
    split(" ", each.value.value)[1]
    : each.value.value
  ) : null

  dynamic "data" {
    for_each = each.value.type == "CAA" ? [split(" ", each.value.value)] : []

    content {
      flags = data.value[0]
      tag   = data.value[1]
      value = data.value[2]
    }
  }

  priority = each.value.type == "MX" ? split(" ", each.value.value)[0] : null
  type     = each.value.type
  ttl      = 1

  allow_overwrite = var.allow_overwrite
}
