# DNS management for Cloudflare zones

Creates DNS records from "CSV zone" files on [Cloudflare]. Data is read from a `zones` folder (path can be configured).
Each file is named by the zone plus `.csv`. (i.e. `example.com` -> `zones/example.com.csv`)

file format:

```csv
type,name,value
A,@,1.2.3.4
CNAME,www,other.example.org
CAA,@,128 issue letsencrypt.org
```

[Cloudflare]: https://cloudflare.com

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.8 |
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | ~> 4.51 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | ~> 4.51 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [cloudflare_record.records](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |
| [cloudflare_zone.zones](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/data-sources/zone) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_overwrite"></a> [allow\_overwrite](#input\_allow\_overwrite) | whether existing records should be overwritten | `bool` | `false` | no |
| <a name="input_zones_path"></a> [zones\_path](#input\_zones\_path) | path to csv files with records | `string` | `"zones"` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->
