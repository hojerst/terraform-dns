variable "zones_path" {
  type        = string
  description = "path to csv files with records"
  default     = "zones"
}

variable "allow_overwrite" {
  type        = bool
  description = "whether existing records should be overwritten"
  default     = false
}
